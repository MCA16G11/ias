<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KeyAspect;
use App\AuditScore;


class DepartmentAudit extends Model
{
  public function department(){
    return $this->belongsTo('App\Department');
  }

  public function collegeAudit(){
    return $this->belongsTo('App\CollegeAudit');
  }

  public function getAuditeeNamesAttribute(){
    $auditee_names = "";
    foreach ($this->auditee as $auditee) {
      $auditee_names .= $auditee->fullname.", ";
    }
    return rtrim($auditee_names, ", ");
   }

   public function getAuditorNamesAttribute(){
    $auditor_names = "";
    foreach ($this->auditor as $auditor) {
      $auditor_names .= $auditor->fullname.", ";
    }
    return rtrim($auditor_names, ", ");
   }
    

      public function auditor(){
        return $this->belongsToMany('App\User', 'department_audit_auditors');
      }

      public function auditee(){
        return $this->belongsToMany('App\User', 'department_audit_auditee');
      }

      public function advisor(){
        return $this->belongsToMany('App\User', 'department_audit_faculty_advisor');
      }

      public function iac(){
        return $this->belongsToMany('App\User', 'department_audit_member_iac');
      }

      public function classCommitee(){
        return $this->belongsToMany('App\User', 'department_audit_member_class_commitee');
      }

      public function auditScore(){
        return $this->hasMany('App\AuditScore');
      }

      public function generateKeyAspects(){
        if ($this->auditScore()->count() > 0)
          return;
        foreach(KeyAspect::all() as $keyAspect){
            $auditScore = new AuditScore;
            $this->auditScore()->save($auditScore);
            $keyAspect->auditScore()->save($auditScore);
        }
      }
}
