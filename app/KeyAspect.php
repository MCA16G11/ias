<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeyAspect extends Model
{
    public function auditScore(){
        return $this->hasMany('App\AuditScore');
    }
}
