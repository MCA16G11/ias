<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditScore extends Model
{
   public function keyAspect(){
       return $this->belongsTo('App\KeyAspect');
   }
   
}
