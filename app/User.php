<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    public function department(){
      return $this->belongsTo('App\Department');
    }

    public function departmentAudit(){
        return $this->belongsToMany('App\DepartmentAudit','department_audit_auditors');
    }

    public function getFullnameAttribute(){
        return $this->name." ".$this->lname;
    }

    public function getIsAuditorAttribute(){
        return ($this->departmentAudit->count() > 0 ? true : false);
    }

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function getIsAmrAttribute(){
        foreach ($this->roles as $role) {
            if ($role->name == "AMR") {
              return true;
            }
        }
        return false;
    }

    public function getIsIQACAttribute(){
        foreach ($this->roles as $role) {
            if ($role->name == "IQAC") {
              return true;
            }
        }
        return false;
    }

    public function getIsIQACCoAttribute(){
        foreach ($this->roles as $role) {
            if ($role->name == "IQACCo") {
              return true;
            }
        }
        return false;
    }

    public function getIsAuditeeAttribute(){
        foreach ($this->roles as $role) {
            if ($role->name == "Auditee") {
              return true;
            }
        }
        return false;
    }

    public function getRoleNamesAttribute(){
        $role_names = "";
        foreach ($this->roles as $role) {
            $role_names .= $role->name.", ";
        }
        return rtrim($role_names, ", ");
    }

    

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
