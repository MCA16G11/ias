<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeAudit extends Model
{

  public function departmentAudit(){
    return $this->hasMany('App\DepartmentAudit');
  }

  protected $fillable = [
      'audit_no', 'from', 'to',
  ];
}
