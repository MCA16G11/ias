<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
  public function head(){
    return $this->hasOne('App\User');
  }

  public function auditee(){
    return $this->belongsToMany('App\User', 'department_auditee');
  }

  public function departmentAudit(){
    return $this->hasOne('App\DepartmentAudit');
  }
}
