<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\User;
use App\CollegeAudit;
use App\DepartmentAudit;
use PDF;

class AuditorSelectionController extends Controller
{
    //show department audit
    public function showDepartmentAudit(Request $request){
        $collegeAudit = CollegeAudit::find($request->college_audit);
        return view('AMR.select_auditor')->with('CollegeAudit', $collegeAudit);
    }
    
    //show shedule
    public function viewSchedule(Request $request){
        $collegeAudit = CollegeAudit::find($request->college_audit);
        return view('AMR.view_schedule')->with('CollegeAudit', $collegeAudit);
    }

    //show IQAC shedule page
    public function viewIQACSchedule(Request $request){
        $collegeAudit = CollegeAudit::find($request->college_audit);
        return view('IQAC.view_schedule')->with('CollegeAudit', $collegeAudit);
    }

    //show IQACCo shedule page
    public function viewIQACCoSchedule(Request $request){
        $collegeAudit = CollegeAudit::find($request->college_audit);
        return view('IQACCo.view_schedule')->with('CollegeAudit', $collegeAudit);
    }

    //show AUDITEE schedule
    public function viewAuditeeSchedule(Request $request){
        $collegeAudit = CollegeAudit::find($request->college_audit);
        return view('AUDITEE.view_schedule')->with('CollegeAudit', $collegeAudit);
    }

    //convert to pdf
    public function pdf($id){
       // $collegeAudit=CollegeAudit::find($id);
       //  $pdf =PDF::loadView('viewShedule',$CollegeAudit);
        PDF::loadHTML($this->schedulePdf($id))->setPaper('a4', 'portrait')->setWarnings(false)->save(public_path().'/schedule.pdf');
         return response()->download(public_path().'/schedule.pdf');

    }

    //making html of view shedules
    public function schedulePdf($id){
          $collegeAudit=CollegeAudit::find($id);
        $output = '
        <html>
        <head>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <style>
              .center{
                text-align:center;
              }
            </style>
        </head>
        <body>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2"><img style="max-width:150px; margin-top: -10px; margin-left: 20px; max-height:130px;" src="mesce1.png"></th>
                        <th colspan="2"><h2 class="center">MES COLLEGE OF ENGINEERING</h2><h4 class="center">KUTTIPPURAM</h4><h5 class="center">KTU-INTERNAL AUDIT SCHEDULE CUM CIRCULAR</h5></th>
                        <th colspan="2"><h4>Audit No: '.$collegeAudit->audit_no.'</h4></th>
                    </tr>
                    <tr>
                      <th>ID</th>
                      <th>DEPARTMENT</th>
                      <th>AUDITEE</th>
                      <th>AUDITOR</th>
                      <th>DATE</th>
                      <th>TIME</th>
                    </tr>
              </thead>
          <tbody>';
          foreach($collegeAudit->departmentAudit as $departmentAudit){
                $output .='<tr>
                <td>'. $departmentAudit->department->id.'</td>
                <td>'. $departmentAudit->department->name.'</td>
                <td>'. $departmentAudit->auditee_names.'</td>
                <td>'. $departmentAudit->auditor_names.'</td>
                <td>'. $departmentAudit->date.'</td>
                <td>'. $departmentAudit->time.'</td>
              </tr>';
        }
      $output .= '</tbody></table></body></html>';
      return $output;
    }

    //store time and date
    public function storeDate(Request $request)
    {
        $collegeAudit = CollegeAudit::find($request->college_audit);
        $department_audit = DepartmentAudit::find($request->department_audit);
        $department_audit->date = $request->from;
        $department_audit->time = $request->slot;
        $department_audit->save();

        return view('AMR.select_auditor')->with('msg','Date and Time setted...')->with('CollegeAudit',$collegeAudit);

    }
    
    //setting auditor of the department
    public function departmentAuditor(Request $request){
        
        $collegeAudit = CollegeAudit::find($request->college_audit);
        $user = User::find($request->staffs);
        $department_Auditor = DepartmentAudit::find($request->department_audit);

        //Attach auditor
        if($request->action == 'attach'){
            if ($department_Auditor->department == $user->department) {
                return view('AMR.select_auditor')->with('msg', 'Select another staff')->with('CollegeAudit',$collegeAudit);
            }
            $department_Auditor->auditor()->attach($user);
            return view('AMR.select_auditor')->with('msg', 'Successfully attached')->with('CollegeAudit',$collegeAudit);
        }

        //Detach auditor
        if($request->action == 'detach'){
            $department_Auditor->auditor()->detach($user);
            return view('AMR.select_auditor')->with('msg', 'Successfully detached')->with('CollegeAudit',$collegeAudit);
        }
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('AMR.select_auditor');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $user = new Role;
        // $user->user_id = $request->usertype;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        // $registers =DB::select('select * from users where status="Approved" and type!="amr"');
        $registers = User::where('status', 'Approved')->get();
        // dump($registers);
        // return;
        return view('AMR.select_auditor',['registers'=>$registers]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function setAuditor($id)
    {

        // $registers=user::find($id);
        // $registers->type='auditee';
        // $registers->save();
        // return back()->with('success','Type Updated as AUDITEE');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//     public function destroy($id)
//     {
//       user::where('id','=',$id)->delete();
//       return back();
//     }
}