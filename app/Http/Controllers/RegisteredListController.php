<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\User;

class RegisteredListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('AMR.staff_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $user = new Role;
        // $user->user_id = $request->usertype;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
        // $registers =DB::select('select * from users where status="Approved" and type!="amr"');
        $registers = User::where('status', 'Approved')->get();
        // dump($registers);
        // return;
        return view('AMR.staff_list',['registers'=>$registers]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setIQAC($id)
    {

        $registers=user::find($id);
        $registers->type='iqac';
        $registers->save();
        return back()->with('success','Type Updated as IQAC');
    }

    public function setAuditee($id)
    {

        $registers=user::find($id);
        $registers->type='auditee';
        $registers->save();
        return back()->with('success','Type Updated as AUDITEE');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      user::where('id','=',$id)->delete();
      return back();
    }
}
