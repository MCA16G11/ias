<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;
use Auth;
class CustomLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

     protected function redirectTo($view)
     {

           return redirect()->route($view);
     }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');
    }

    //function to override built in login function
    public function Login(Request $request)
    {
      $request->validate([
        'email'=>'required|email',
        'psw' => 'required',
      ]);
      $user = new User;
      $loginUser = $user->where('email','=',$request->email)->first();


      if(isset($loginUser->id))
      {
        switch ($loginUser->type) {

          case 'amr':
            $view = 'AMR.amrhome';
            break;

          case 'auditee':
            $view = 'AUDITEE.auditee_home';
            break;
          case 'auditor':
            $view = 'AUDITOR.auditor_home';
            break;
          case 'iqac':
            $view = 'IQAC.iqac_home';
            break;
          default:
            $view = '/';
            break;
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->psw, 'status' => 'Approved'])) {
          return redirect()->route($view);
        }
        else{
          Session::flash("credentials not found ");
          return back();
        }
      }
      else{
        Session::flash("credentials not found ");
        return back();
      }
    }
}
