<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DepartmentAudit;
use App\AuditScore;
use App\CollegeAudit;
use App\KeyAspect;

class KeyAspectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {
        // $request->validate([
        //     'auditno' => 'required',
        //     'dept' => 'required',
        // ]);

        // $keyAspect = KeyAspect::all();
        // dump($keyAspect);
        // return;
        //generating keyaspects
        // dump($request);
        // return;
        $departmentAudit = DepartmentAudit::find($request->departmentAudit);
        // dump($departmentAudit);return;
        $departmentAudit->generateKeyAspects();
        return view('AUDITOR.form3')->with('departmentAudit', $departmentAudit);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function keyAspectupdate(Request $request)
    { 
        

        $departmentAudit = DepartmentAudit::find($request->departmentAudit);
        foreach ($departmentAudit->auditScore as $auditScore) {
            $input_mark = 'mark_'.$auditScore->id;
            $input_remark = 'remark_'.$auditScore->id;
            $auditScore->mark = $request->$input_mark;
            $auditScore->remark = $request->$input_remark;
            $auditScore->save();
            
        }
        $departmentAudit->completed = true;
        $departmentAudit->save();
        return view('AUDITOR.auditor_home');
        
    }

    public function viewReport(Request $request){

        $collegeAudit = CollegeAudit::find($request->college_audit);
        return view('AMR.view_report')->with('CollegeAudit', $collegeAudit);
    }

    //IQACCo home Report
    public function resultIQACCo(Request $request){

        $collegeAudit = CollegeAudit::find($request->college_audit);
        return view('IQACCo.view_report')->with('CollegeAudit', $collegeAudit);
    }

    public function departmentReportIQACCo(Request $request){

        $departmentAudit = DepartmentAudit::find($request->departmentAudit);
        //$request->departmentAudit
        // dump($departmentAudit->auditScore);return;
        return view('IQACCo.view_report1')->with('departmentAudit', $departmentAudit);
    }

    public function departmentReport(Request $request){

        $departmentAudit = DepartmentAudit::find($request->departmentAudit);
        //$request->departmentAudit
        // dump($departmentAudit->auditScore);return;
        return view('AMR.view_report1')->with('departmentAudit', $departmentAudit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
