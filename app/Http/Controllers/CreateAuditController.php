<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\CollegeAudit;
use App\Department;
use App\DepartmentAudit;

class CreateAuditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('AMR.create_audit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {

      $request->validate([
        'auditno' => 'required|string|max:15',
        'from' => 'required|date|',
        'to' => 'required|date',
        ]);

         $CollegeAudits = new CollegeAudit;
         $CollegeAudits->audit_no = $request->auditno;
         $CollegeAudits->from = $request->from;
         $CollegeAudits->to = $request->to;
         $CollegeAudits->save();

         //Create DepartmentAudits
         foreach (Department::all() as $department) {
            $departmentAudit = new DepartmentAudit;
            $department->departmentAudit()->save($departmentAudit);
            $CollegeAudits->departmentAudit()->save($departmentAudit);

            //Set Auditee
            foreach ($department->auditee as $auditee) {
                $departmentAudit->auditee()->attach($auditee);
            }
         }
        Session::flash("msg","Audit Created Successfully");
         return view('AMR.create_audit')->with('collegeAudit',$CollegeAudits);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAudit($id)
    {
        $CollegeAudits = new CollegeAudit;
        $collegeAudit = CollegeAudit::find($id);

        foreach($collegeAudit->departmentAudit as $departmentAudit){
            $departmentAudit->auditee()->detach();
        }
        $collegeAudit->departmentAudit()->delete();
        $collegeAudit->delete();
        Session::flash("msg","Audit deleted Successfully");
        return view('AMR.create_audit')->with('collegeAudit',$CollegeAudits);
    }
}
