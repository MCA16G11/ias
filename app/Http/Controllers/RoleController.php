<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dump($request);
        // return;
        
        $role = Role::find($request->role); 
        $user = User::find($request->user);
        //echo $role;
       //echo $user->role_names;

       //Attach role
        if($request->action == 'attach'){
            foreach($user->roles as $currentrole){
                if($role->name==$currentrole->name){
                    
                    return back()->with('msg','Role allready exist');
                }
            }
            $user->roles()->attach($role);
            if ($role->name == 'Auditee') {
                $department = $user->department;
                $department->auditee()->attach($user);
                return back()->with('msg','Successfully Attached..');
            }
            return back()->with('msg','Successfully Attached..');
        }

        //Dettach role
        if($request->action == 'detach'){
            foreach($user->roles as $currentrole){
                if($role->name==$currentrole->name){
                    if ($role->name == 'Auditee') {
                        $department = $user->department;
                        $department->auditee()->detach($user);
                    }                    
                    $user->roles()->detach($role);

                    return back()->with('msg','Successfully Detached..');
            }
          }
        
            return back()->with('msg','Select an existing role');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
     {
    //     $role = Role::find($request->role); 
    //     $user = User::find($request->user);
    //     $user->roles()->dettach($role);
    //   return back();
    }
}
