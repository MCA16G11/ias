<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\notification;

class NotificationController extends Controller
{
    public function sendNotification(Request $request){
    	//dump($request);return;
    	$request->validate([
    	'notificationsubject' => 'required|string|max:255'
    	]);
    	$notification = new notification;
    	$notification->notification = $request->notificationsubject;
    	$notification->save();

    	Session::flash("msg","Notification Sended Successfully");
    	return view('IQACCo.send_notification')->with('notification',$notification);
    }
}
