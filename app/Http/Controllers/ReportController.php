<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\DepartmentAudit;

class ReportController extends Controller
{
    
    public function Pdf($id){
    	PDF::loadHTML($this->reportPdf($id))->setPaper('a4', 'portrait')->setWarnings(false)->save(public_path().'/report.pdf');
         return response()->download(public_path().'/report.pdf');
    }

    public function reportPdf($id){

		$departmentAudit = DepartmentAudit::find($id);
    	$output = '
        <html>
        <head>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <style>
              .center{
                text-align:center;
              }
            </style>
        </head>
        <body>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="1"><img style="max-width:150px; margin-top: -10px; margin-left: 20px; max-height:130px;" src="mesce1.png"></th>
                        <th colspan="2"><h2 class="center">MES COLLEGE OF ENGINEERING</h2><h4 class="center">KUTTIPPURAM</h4><h5 class="center">KTU-INTERNAL AUDIT REPORT</h5></th>
                        <th colspan="2">Department: '.$departmentAudit->department->name.'</h5></th>
                    </tr>
                    <tr>
                      <th>ID</th>
                      <th>KEY ASPECT</th>
                      <th>GROUP</th>
                      <th>SCORE</th>
                      <th>REMARKS</th>
                    </tr>
                </thead>
              <tbody>';
          foreach($departmentAudit->auditScore as $auditScore){
          	$output .='<tr>
                  <td>'.$auditScore->keyAspect->id.'</td>
                  <td>'.$auditScore->keyAspect->title.'</td>
                  <td>'.$auditScore->keyAspect->group.'</td>
                  <td>'.$auditScore->mark.'</td>
                  <td>'.$auditScore->remark.'</td>
                </tr>';
    }
    $output .= '</tbody></table></body></html>';
      return $output;
   }
}
