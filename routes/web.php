<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('test', 'TestController@test');
Route::get('/', function () {
    return view('auth.login');
});

Route::get('/main_home', function () {
    return view('main_home');
});

Route::get('/contact', function () {
    return view('contact');
});

// Route::get('/contact','MessageController@index');
Route::get('/storeContact','MessageController@storeContact');
Route::get('/AMR.view_messages','MessageController@showContact');



Route::get('/about', function () {
    return view('about');
});

Route::get('/login', function () {
    return view('login');
});

Route::get('/aaa', function () {
    return view('aaa');
});

Route::get('/register1', function () {
    return view('register1');
});

Route::get('/schedule', function () {
    return view('schedule');
});

Route::get('/pending', function () {
    return view('pending');
});


//AMR TABS
Route::get('/amrhome', function () {
    return view('AMR.amrhome');
})->name('AMR.amrhome');

Route::get('/pending_list', function () {
    return view('AMR.pending_list');
})->middleware('auth');

Route::get('/staff_list', function () {
    return view('AMR.staff_list');
})->middleware('auth');

Route::get('/create_audit', function () {
    return view('AMR.create_audit');
})->middleware('auth');

Route::get('/view_schedule', function () {
    return view('AMR.view_schedule');
});


Route::get('/select_auditor', function () {
    return view('AMR.select_auditor');
});

Route::get('/auditor_list', function () {
    return view('AMR.auditor_list');
});

Route::get('/notification', function () {
    return view('AMR.notification');
});

Route::get('/view_messages', function () {
    return view('AMR.view_messages');
});

Route::get('/view_report', function () {
    return view('AMR.view_report');
});

Route::get('/view_report1', function () {
    return view('AMR.view_report1');
});


//AUDITEE TABS
Route::get('/AUDITEE.auditee_home', function () {
    return view('AUDITEE.auditee_home');
})->name('AUDITEE.auditee_home');

Route::get('/AUDITEE.auditee_profile', function () {
    return view('AUDITEE.auditee_profile');
});

Route::get('/AUDITEE.notification', function () {
    return view('AUDITEE.notification');
});

Route::get('/AUDITEE.view_generalinfo', function () {
    return view('AUDITEE.view_generalinfo');
});

Route::get('/AUDITEE.view_checklist', function () {
    return view('AUDITEE.view_checklist');
});

Route::get('/AUDITEE.view_report', function () {
    return view('AUDITEE.view_report');
});

Route::get('/AUDITEE.view_schedule', function () {
    return view('AUDITEE.view_schedule');
});

//IQAC TABS
Route::get('/IQAC.iqac_home', function () {
    return view('IQAC.iqac_home');
})->name('IQAC.iqac_home');

Route::get('/IQAC.iqac_profile', function () {
    return view('IQAC.iqac_profile');
});

Route::get('/IQAC.notification', function () {
    return view('IQAC.notification');
});

Route::get('/IQAC.view_schedule', function () {
    return view('IQAC.view_schedule');
});

Route::get('/IQAC.view_auditors', function () {
    return view('IQAC.view_auditors');
});

Route::get('/IQAC.view_report', function () {
    return view('IQAC.view_report');
});


//IQAC COORDINATOR TABS
Route::get('/IQACCo.iqacco_home', function () {
    return view('IQACCo.iqacco_home');
})->name('IQACCo.iqacco_home');

Route::get('/IQACCo.iqacco_profile', function () {
    return view('IQACCo.iqacco_profile');
});

Route::get('/IQACCo.send_notification', function () {
    return view('IQACCo.send_notification');
});

Route::get('/IQACCo.view_schedule', function () {
    return view('IQACCo.view_schedule');
});

Route::get('/IQACCo.view_auditors', function () {
    return view('IQACCo.view_auditors');
});

Route::get('/IQACCo.view_report', function () {
    return view('IQACCo.view_report');
});

//AUDITOR TABS
Route::get('/AUDITOR.auditor_home', function () {
    return view('AUDITOR.auditor_home');
})->name('AUDITOR.auditor_home');

Route::get('/AUDITOR.notes', function () {
    return view('AUDITOR.notes');
});

Route::get('/AUDITOR.form1', function () {
    return view('AUDITOR.form1');
});

Route::POST('/AUDITOR.form2', function () {
    return view('AUDITOR.form2');
});

Route::get('/AUDITOR.form3', function () {
    return view('AUDITOR.form3');
});

// routes for contact blade.
Route::get('/contact', 'MessageController@index');
Route::post('/storeContact', 'MessageController@storeContact');
Route::get('/view_messages', 'MessageController@showContact');

//route for create audit
Route::get('/create_audit', 'CreateAuditController@index');
Route::post('insert', 'CreateAuditController@insert');
Route::get('/destroyAudit/{id}','CreateAuditController@destroyAudit');
//login route
Route::post('/loginCustom','CustomLoginController@login')->name("loginCustom");


//rout for register blade
Route::get('/register', 'RegisterController@index');  //for display registration form
Route::post('/registration','RegisterController@registration');  //for storing values


//route for pending list
Route::get('/pending_list', 'PendingListController@show');  //for display registration form
Route::get('/update/{id}','PendingListController@update');
Route::get('/destroy/{id}','PendingListController@destroy');

//registered list
Route::get('/staff_list','RegisteredListController@index');
Route::get('/staff_list','RegisteredListController@show');
Route::get('/setIQAC/{id}','RegisteredListController@setIQAC');
Route::get('/setAuditee/{id}','RegisteredListController@setAuditee');

//rout for auditor selection blade
Route::get('/select_auditor','AuditorSelectionController@index');
Route::get('/select_auditor','AuditorSelectionController@show');
Route::post('show_schedule', 'AuditorSelectionController@showDepartmentAudit');
Route::post('store_date', 'AuditorSelectionController@storeDate');
Route::post('role/auditor', 'AuditorSelectionController@departmentAuditor');
Route::post('viewSchedule', 'AuditorSelectionController@viewSchedule');
Route::get('schedulepdf/pdf/{id}', 'AuditorSelectionController@pdf');


//IQAC Home routes
Route::post('viewIQACSchedule', 'AuditorSelectionController@viewIQACSchedule');

//IQACCo Home routes
Route::post('viewIQACCoSchedule', 'AuditorSelectionController@viewIQACCoSchedule');
Route::post('resultIQACCo', 'KeyAspectController@resultIQACCo');
Route::post('view_reportIqacco','KeyAspectController@departmentReportIQACCo');

//AUDITEE Home routes
Route::post('viewAuditeeSchedule', 'AuditorSelectionController@viewAuditeeSchedule');


//Route for RoleController
// Route::get('/AMR.staff_list','RoleController@index');
Route::post('role/store','RoleController@store');

//Route for generating keyAspects
Route::post('generate','KeyAspectController@generate');
Route::post('valuation','KeyAspectController@keyAspectupdate');
Route::post('result','KeyAspectController@viewReport');
Route::post('view_report1','KeyAspectController@departmentReport');
Route::get('report/Pdf/{id}','ReportController@Pdf');
Route::POST('sendNotification','NotificationController@sendNotification');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');




Route::get('/view_report1', function () {
    return view('AMR.view_report1');
});