<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
        
        'staffid'=>'1001',
        'name'=>'Mohammed',
        'lname'=>'Shabeer',
        'department_id'=>1,
        'year'=>2001,
        'email'=>'shabeerklr94@gmail.com',
        'password'=> bcrypt('123456')
       ]);

       DB::table('users')->insert([
        
        'staffid'=>'1002',
        'name'=>'Shameem',
        'lname'=>'Rahman',
        'department_id'=>2,
        'year'=>2001,
        'email'=>'shameem@gmail.com',
        'password'=> bcrypt('123456')
       ]);

       DB::table('users')->insert([
        
        'staffid'=>'1003',
        'name'=>'Anjali',
        'lname'=>'kp',
        'department_id'=>3,
        'year'=>2001,
        'email'=>'anjali@gmail.com',
        'password'=> bcrypt('123456')
       ]);
       DB::table('users')->insert([
        
        'staffid'=>'1004',
        'name'=>'Bharath',
        'lname'=>'D',
        'department_id'=>4,
        'year'=>2001,
        'email'=>'bharath@gmail.com',
        'password'=> bcrypt('123456')
       ]);

       DB::table('users')->insert([
        
        'staffid'=>'1005',
        'name'=>'Abdul',
        'lname'=>'Samad',
        'department_id'=>5,
        'year'=>2001,
        'email'=>'samad@gmail.com',
        'password'=> bcrypt('123456')
       ]);

       DB::table('users')->insert([
        
        'staffid'=>'1006',
        'name'=>'Anjala',
        'lname'=>'k',
        'department_id'=>6,
        'year'=>2006,
        'email'=>'anjala@gmail.com',
        'password'=> bcrypt('123456')
       ]);

       DB::table('users')->insert([
        
        'staffid'=>'1007',
        'name'=>'Anjana',
        'lname'=>'k',
        'department_id'=>7,
        'year'=>2009,
        'email'=>'anjana@gmail.com',
        'password'=> bcrypt('123456')
       ]);

       DB::table('users')->insert([
        
        'staffid'=>'1008',
        'name'=>'Rifana',
        'lname'=>'M T',
        'department_id'=>8,
        'year'=>2005,
        'email'=>'rifana@gmail.com',
        'password'=> bcrypt('123456')
       ]);

       DB::table('users')->insert([
        
        'staffid'=>'1009',
        'name'=>'Anusha',
        'lname'=>'K P',
        'department_id'=>9,
        'year'=>2014,
        'email'=>'anusha@gmail.com',
        'password'=> bcrypt('123456')
       ]);

       DB::table('users')->insert([
        
        'staffid'=>'1010',
        'name'=>'Safa',
        'lname'=>'K',
        'department_id'=>10,
        'year'=>2015,
        'email'=>'safa@gmail.com',
        'password'=> bcrypt('123456')
       ]);
    }
}
