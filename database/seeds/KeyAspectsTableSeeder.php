<?php

use Illuminate\Database\Seeder;

class KeyAspectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('key_aspects')->insert([
          'title' => 'Compliance to the Academic Calendar of KTU',
          'group' => 'B'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Functioning of class/course committees',
          'group' => 'B'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Schedule of Time',
          'group' => 'B'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Attendance of students',
          'group' => 'B'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Quantity & Quality of Assignments/Mini Projects',
          'group' => 'C'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Conduct of Tutorial Classes',
          'group' => 'A'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Subject coverage as per course plan',
          'group' => 'B'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Use of ICT enabled teaching & Digital courses',
          'group' => 'C'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Learning materials and software available for student learning'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Quality, coverage etc. of question papers of internal Exam',
          'group' => 'B'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Evaluation of internal exams',
          'group' => 'B'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Conduct of Practical classes',
          'group' => 'A'
      ]);
      DB::table('key_aspects')->insert([
          'title' => ' Syllabus coverage of practical courses',
          'group' => 'A'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Evaluation of students performance in practical classes',
          'group' => 'A'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Conduct of seminar classes',
          'group' => 'A'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Quality & Progress of projects',
          'group' => 'A'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Conduct of Re-tests'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Performance of students in Internal exams'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Conduct of remedial classes',
          'group' => 'C'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Maintenance of course diary',
          'group' => 'A'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Maintenance of course file'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Award of internal evaluation marks'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Performance of students in the previous semester',
          'group' => 'A'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Eligibility for promotion to higher semesters at the
        end of even semesters'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Faculty evaluation by students',
          'group' => 'A'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Conduct of expert talks, industry interaction etc. for
         students'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Facility for  co-curricular activities of students'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Facility for  extracurricular activities of students '
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Functioning of students grievances and appeal committee',
          'group' => 'B'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Functioning of Academic Discipline & Welfare committee',
          'group' => 'B'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Information on progress of students to their parents',
          'group' => 'C'
      ]);
      DB::table('key_aspects')->insert([
          'title' => ' Placement Activities'
      ]);
      DB::table('key_aspects')->insert([
          'title' => 'Entrepreneurship Development Activities'
      ]);
    }
}
