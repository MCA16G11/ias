<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert([
          'name' => 'AMR'
      ]);
      DB::table('roles')->insert([
          'name' => 'IQAC'
      ]);
      DB::table('roles')->insert([
          'name' => 'IQACCo'
      ]);
      DB::table('roles')->insert([
          'name' => 'Auditee'
      ]);

    }
}
