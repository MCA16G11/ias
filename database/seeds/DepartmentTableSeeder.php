<?php

use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('departments')->insert([
          'name' => 'AEI',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'ARCH',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'CIVIL',
         
      ]);
      DB::table('departments')->insert([
          'name' => 'CSE',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'ECE',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'EEE',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'IT',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'ME',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'MBA',
          
      ]);
      DB::table('departments')->insert([

          'name' => 'MCA',
          'hod'=>1
          
      ]);
      DB::table('departments')->insert([
          'name' => 'MATHS',
          'hod'=>2
         
      ]);
      DB::table('departments')->insert([
          'name' => 'S&H',
          'hod'=>3
      ]);
      DB::table('departments')->insert([
          'name' => 'T&P CELL',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'EXAMCELL',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'LIBRARY',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'Phy.Edn',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'EDC',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'CCF',
          
      ]);
      DB::table('departments')->insert([
          'name' => 'NSS U1',
         
      ]);
      DB::table('departments')->insert([
          'name' => 'NSS U2',
         
      ]);
    }
}
