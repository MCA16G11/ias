<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('college_audit_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->string('course')->nullable();
            $table->date('date_of_audit')->nullable();
            $table->string('class')->nullable();
            $table->string('baranch')->nullable();
            $table->string('batch')->nullable();
            $table->boolean('accreditation_body')->nullable();
            $table->boolean('iso')->nullable();
            $table->string('grade_awarded')->nullable();
            $table->date('valid_upto')->nullable();
            $table->integer('no_of_students')->nullable();
            $table->integer('miniprojects')->nullable();
            $table->integer('majorprojects')->nullable();
            $table->string('time')->nullable();
            $table->date('date')->nullable();
            $table->boolean('completed')->default(false);

            $table->timestamps();
        });

        Schema::create('department_audit_auditors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_audit_id');
            $table->integer('user_id');
            
        });

        Schema::create('department_audit_auditee', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_audit_id');
            $table->integer('user_id');
        });

        Schema::create('department_audit_faculty_advisor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_audit_id');
            $table->integer('user_id');
        });

        Schema::create('department_audit_member_iac', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_audit_id');
            $table->integer('user_id');
        });

        Schema::create('department_audit_member_class_commitee', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_audit_id');
            $table->integer('user_id');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_audits');
        Schema::dropIfExists('department_audit_auditors');
        Schema::dropIfExists('department_audit_auditee');
        Schema::dropIfExists('department_audit_faculty_advisor');
        Schema::dropIfExists('department_audit_member_iac');
        Schema::dropIfExists('department_audit_member_class_commitee');
        
    }
}
