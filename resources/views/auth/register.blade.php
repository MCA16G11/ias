<!DOCTYPE html>
<html>
<title>Registration</title>
<head>

  <title>Sign-Up/Login Form</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style>
*{
  box-sizing: border-box;
  margin:0px;
  padding:0px;
}

body {
  background:url(audit3.jpg);
  font-family: 'Titillium Web', sans-serif;
  background-size:cover;
}

a {
  text-decoration: none;
  color: #0008;
  transition: .5s ease;
}
a:hover {
  color: #179b77;
}

.form {
  padding: 10px;
  max-width: 500px;
  max-height: 600px;
  margin: 10px auto;
  border-radius: 4px;
  box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
  transition: .5s ease;
}

.form:hover {
	box-shadow: 0px 0px 40px 16px rgba(18,18,18,1.00);
	}

.tab-group {
  list-style: none;
  padding: 0;
  margin: 0 0 40px 0;
}
.tab-group:after {
  content: "";
  display: table;
  clear: both;
}
.tab-group li a {
  display: block;
  text-decoration: none;
  padding: 15px;
  background: #000;
  color: #ffffff;
  font-size: 20px;
  float: left;
  width: 50%;
  text-align: center;
  cursor: pointer;
  transition: .5s ease;
}
.tab-group li a:hover {
  background: #179b77;
  color: #ffffff;
}
.tab-group .active a {
  background: #1ab188;
  color: #ffffff;
}

.tab-content > div:last-child {
  display: none;
}

h1 {
  text-align: center;
  color: #0008;
  font-weight: 300;
  margin: 0 0 40px;
}

label {
  position: absolute;
  transform: translateY(6px);
  left: 13px;
  color: #0008;
  transition: all 0.25s ease;
  pointer-events: none;
  font-size: 22px;
}

label.active {
  transform: translateY(50px);
  left: 2px;
  font-size: 14px;
}
label.active .req {
  opacity: 0;
}

label.highlight {
  color: #000;
  margin-top:-10px;
}

input {
  font-size: 22px;
  display: block;
  width: 100%;
  height: 100%;
  padding: 5px 10px;
  background: none;
  background-image: none;
  border: 1px solid #fff;
  color: #0008;
  border-radius: 0;
  transition: border-color .25s ease, box-shadow .25s ease;
}
input:focus{
  outline: 0;
  border-color: #0008;
}

.field-wrap {
  position: relative;
  margin-bottom: 40px;
}

.top-row:after {
  content: "";
  display: table;
  clear: both;
}
.top-row > div {
  float: left;
  width: 48%;
  margin-right: 4%;
}
.top-row > div:last-child {
  margin: 0;
}

.button {
  border: 0;
  outline: none;
  border-radius: 0;
  padding: 15px 0;
  font-size: 2rem;
  font-weight: 600;
  text-transform: uppercase;
  letter-spacing: .1em;
  background: #1ab188;
  color: #ffffff;
  transition: all 0.5s ease;
}
.button:hover, .button:focus {
  background: #179b77;
}

.button-block {
  display: block;
  width: 100%;
}


/* Style the navbar */
#navbar {
  overflow: hidden;
  background-color: #333;
}

/* Navbar links */
#navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px;
  text-decoration: none;
}

/* Page content */
.content {
  padding: 120px 500px;
}

/* The sticky class is added to the navbar with JS when it reaches its scroll position */
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

/* Add some top padding to the page content to prevent sudden quick movement (as the navigation bar gets a new position at the top of the page (position:fixed and top:0) */
.sticky + .content {
  padding-top: 60px;
}

/* Style the navigation bar */
.navbar {
  width: 100%;
  background-color: #0008;
  overflow: auto;
}

/* Navbar links */
.navbar a {
  float: left;
  text-align: center;
  padding: 12px;
  color: white;
  text-decoration: none;
  font-size: 17px;
}

/* Navbar links on mouse-over */
.navbar a:hover {
  background-color: #000;
}


/* Add responsiveness - will automatically display the navbar vertically instead of horizontally on screens less than 500 pixels */
@media screen and (max-width: 500px) {
  .navbar a {
    float: none;
    display: block;
  }
}

.top-right {
                position: absolute;
                right: 10px;
                top: 7px;
            }


.footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    height: 6%;
    background-color: #0008;
    color: black;
    text-align: center;
    padding:12px;
}

</style>
</head>

<body>

<div class="navbar">
  <a href="{{url('/')}}"><i class="fa fa-fw fa-home"></i> Home</a>
 </div>

  <div class="form">

      <!-- <ul class="tab-group">
        <li class="tab active"><a href="#register">Register</a></li>
      </ul>
       -->
      <div class="tab-content">
        <div id="register">
          <h1>Register</h1>
          @if ($errors->any())
          <div class="alert alert-danger">
          <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if(Session::has('msg'))
        <h4>{{session::get('msg')}}</h4>
        @endif
          <form action="{{url('store')}}" method="post">

          <div class="top-row">
            <div class="field-wrap">
              <label>
                First Name
              </label>
              <input type="text" required autocomplete="off" name="fname"/>
            </div>

            <div class="field-wrap">
              <label>
                Last Name
              </label>
              <input type="text"required autocomplete="off" name="lname"/>
            </div>
          </div>

		  <div class="top-row">
            <div class="field-wrap">
              <label>
                Staff Id
              </label>
              <input type="text" required autocomplete="off" name="stid"/>
            </div>

            <div class="field-wrap">
              <label>
                Year
              </label>
              <input type="text"required autocomplete="off" name="year"/>
            </div>
          </div>

            <div class="field-wrap">
              <label>
                Department
              </label>
              <input type="text"required autocomplete="off" name="dept"/>
            </div>
          </div>


          <div class="field-wrap">
            <label>
              Email Address
            </label>
            <input type="email"required autocomplete="off" name="email"/>
          </div>

          <div class="field-wrap">
            <label>
              Set A Password
            </label>
            <input type="password"required autocomplete="off" name="pswd"/>
          </div>
          <input class="input2" type="hidden" name="_token" value="{{csrf_token()}}">
          <button type="submit" class="button button-block"/>Register</button>

          </form>

        </div>

          </form>

        </div>

      </div><!-- tab-content -->

</div> <!-- /form -->


<div class="footer">
  <p>Copy right protected by BiGOne IT SolutionS</p>
  </div>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script>

$('.form').find('input').on('keyup blur focus', function (e) {

  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight');
			} else {
		    label.removeClass('highlight');
			}
    } else if (e.type === 'focus') {

      if( $this.val() === '' ) {
    		label.removeClass('highlight');
			}
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {

  e.preventDefault();

  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');

  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();

  $(target).fadeIn(600);

});

</script>

</body>
</html>
