<!doctype html>
<html>
<head>
<title>IASMESCE</title>

<link rel="shortcut icon" href="IASttl1.png"/>
<!-- Load an icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('css/welcome.css') }}">
</head>

<style>
html, body{
background-image: url(audit3.jpg);
               background-size: cover;
               opacity: inherit;
               color: #636b6f;
               font-family: 'Raleway', sans-serif;
               font-weight: 100;
               height: 100vh;
               margin: 0;
           }
</style>


<body>
<!-- Navbar -->
<div class="navbar">
  <a class="active" href="{{url('/')}}"><i class="fa fa-fw fa-home"></i> Home</a>

  <div class="top-right">
  <button class="open-button"onclick="openForm()"><i class="fa fa-fw fa-user"></i>Login</button>
  </div>
</div>

<!-- Logo Animation -->
<div class="content">
<div class="container">
<div class="flip-card">
    <div class="flip-card-inner">
     <div class="flip-card-front">
     <img src="IASLogo.png" width="250" height="200" align: "center">
    </div>
     <div class="flip-card-back">
      <p>Welcome to</p>
     <h1>Internal Auditing System</h1>
      <p>-The team that ticks all</p>
      <p>the boxes-</p>
       </div>
     </div>
    </div>
            
<div class="form-popup" id="myForm">
  <form action="{{ route('login') }}" class="form-container" method="POST">

                        @csrf
                        <h1>Login</h1>
    <div>                    
    <label for="email"><b>Email</b></label>                            
    <input id="email" type="email" placeholder="Enter Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                
    </div>                      

    <div>                    
    <label for="password"><b>Password</b></label>
    <input id="password" type="password" placeholder="Enter Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
    </div>                     

                        <div class="form-group row">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        <button type="submit" class="btn">Login</button>
                        <div class="forgot-pass">
                                <a href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}                                 
                                </a>
                        </div>
                        <button type="button" class="btn cancel" onclick="closeForm()">X</button>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </form>
 </div>
<div id="mySidenav" class="sidenav">
  <a href="{{url('/about')}}" id="about"><i class="fa fa-info-circle"></i> About</a>
  <a href="{{url('/register1')}}" id="register"><i class="fa fa-address-card"></i> Register</a>
  <a href="{{url('/AMR.amrhome')}}" id="projects"><i class="fa fa-calendar"></i> Calendar</a>
  <a href="{{url('/contact')}}" id="contact"><i class="fa fa-address-book"></i> Contact</a>
</div>

 <!-- Log in Form JS -->
<script>

function openForm() {
    document.getElementById("myForm").style.display = "block";
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}

</script>
</body>
</html>
