<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>IQAC-PROFILE</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>IQAC HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="{{ url('IQAC.iqac_profile') }}">Profile</a></li>
        <li><a href="{{ url('IQAC.notification') }}">Notifications</a></li>
        <li><a href="{{ url('IQAC.view_schedule') }}">View Schedule</a></li>
        <li><a href="{{ url('IQAC.view_auditors') }}">View Auditors</a></li>
        <li><a href="{{ url('IQAC.view_report') }}">Audit Report</a></li>
      </ul><br>
    </div>

<!-- Table Content -->
    <div class="col-sm-9">
      <h4><small>IQAC PROFILE</small></h4>
      <div class="hr">
      <hr>
      </div>

    </div>
  </div>


<!-- footer -->
<footer class="container-fluid">
  <p>Powered By BiGOne IT SolutionS</p>
</footer>
</div>
</body>
</html>
