<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>IQAC-AUDIT-SCHEDULE</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">

  <style>
  .center{
    text-align:center;
  }
  .btn1{
    position: absolute;
    top: 70px;
    left: 985px; 
  }

  </style>
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>IQAC HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <!-- <li><a href="{{ url('IQAC.iqac_profile') }}">Profile</a></li> -->
        <li><a href="{{ ('IQAC.notification') }}">Notifications</a></li>
        <li class="active"><a href="{{ url('IQAC.view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('IQAC.view_auditors') }}">View Auditors</a></li> -->
        <!-- <li><a href="{{ url('IQAC.view_report') }}">Audit Report</a></li> -->
      </ul><br>
    </div>

<!-- Table Content -->
    <div class="container">
    <div class="col-sm-9" >
      <h4><small>AUDIT SCHEDULE</small></h4>
      <div class="hr">
      <hr>
      </div>

    <div class="dropdown">
      <form action="{{ url('viewIQACSchedule') }}" class="form-inline" method="post">
        @csrf
      <div class="form-group">
      <label for="dept">SELECT AUDIT NO.
        <select class="form-control" name="college_audit">
          @foreach(App\CollegeAudit::all() as $auditnum)
              <option  value="{{$auditnum->id}}" @isset($CollegeAudit) @if($auditnum == $CollegeAudit) selected @endif @endisset >{{$auditnum->audit_no}}</option>
          @endforeach
        </select>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>

    </div>

    @isset($CollegeAudit)
    <!-- Table Content -->
    <div class="table">
    
            <table class="table table-bordered">
              <thead>
              <tr>
              <th colspan="2"><img style="max-width:150px; margin-top: -10px; margin-left: 20px; max-height:130px;" src="mesce1.png"></th>
              <th colspan="2"><h2 class="center">MES COLLEGE OF ENGINEERING</h2><h4 class="center">KUTTIPPURAM</h4><h5 class="center">KTU-INTERNAL AUDIT SCHEDULE CUM CIRCULAR</h5></th>
              <th colspan="2"><h4>Audit No: {{$CollegeAudit->audit_no}}</h4></th>
              
              </tr>
              <tr>
                <th>ID</th>
                <th>DEPARTMENT</th>
                <th>AUDITEE</th>
                <th>AUDITOR</th>
                <th>DATE</th>
                <th>TIME</th>
              </tr>
            </thead>
            <tbody>
          
              
              @foreach($CollegeAudit->departmentAudit as $departmentAudit)
                <tr>
                  <td>{{$departmentAudit->department->id}}</td>
                  <td>{{$departmentAudit->department->name}}</td>
                  <td>{{$departmentAudit->auditee_names}}</td>
                  <td>{{$departmentAudit->auditor_names}}</td>
                  <td>{{$departmentAudit->date}}</td>
                  <td>{{$departmentAudit->time}}</td>
                </tr>
               @endforeach     
              </tbody>
            </table>
    </div>  

    <div class="btn1">
          <a href="{{ url('schedulepdf/pdf/'.$CollegeAudit->id) }}" class="btn btn-danger" target="_blank"><i class="fa fa-download"></i> Export</a>
          
        </div>
    @endisset    
    </div>
    </div>
  </div>
</div>

<!-- footer -->
<footer class="container-fluid">
  <p>Powered By BiGOne IT SolutionS</p>
</footer>
</div>
</body>
</html>
