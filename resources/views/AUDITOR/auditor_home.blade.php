<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>AUDITOR-HOME</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AUDITOR HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li ><a href="AUDITOR.notes">Notes</a></li>
        <!-- <li ><a href="AUDITOR.form1">form1</a></li> -->
        <!-- <li><a href="AUDITOR.form2">form2</a></li> -->
        <!-- <li><a href="AUDITOR.form3">form3</a></li> -->
      </ul><br>
    </div>

<!-- Table Head -->
    <div class="col-sm-9">
      <h4><small>AUDITOR HOME</small></h4>
      <div class="hr">
      <hr>
      </div>

 <!-- Table Content -->
      <h1>AUDITOR HOME</h1>
      
    </div>
  </div>


<!-- footer -->
<footer class="container-fluid">
  <p><p>&copy Copyright Protected By BiGOne IT SolutionS</p></p>
</footer>
</div>
</body>
</html>
