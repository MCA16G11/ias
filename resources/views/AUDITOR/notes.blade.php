<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>AUDITOR-HOME</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AUDITOR HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="AUDITOR.notes">Notes</a></li>
        <!-- <li ><a href="AUDITOR.form1">form1</a></li> -->
        <!-- <li><a href="AUDITOR.form2">form2</a></li> -->
        <!-- <li><a href="AUDITOR.form3">form3</a></li> -->
      </ul><br>
    </div>

<!-- Table Head -->
    <div class="col-sm-9">
      <h4><small>NOTE TO THE AUDITORS</small></h4>
      <hr>

 <!-- Table Content -->
      <pre>
    
       1. The audit is to be based on the Key Aspects mentioned in the evaluation sheets and in the guidelines.
          (Please verify with the software)
       2. Key aspect evaluation to be on a five point scale as follows
          Excellent (5) 	Good (4) 	Fair (3) 		Poor (2) 	Very Poor (1)
       3.	Separate forms must be used for UG and PG
       4.	Auditors’ comments should be placed in the Remarks / Observations column
       5.	Inform the observed problems to the department during the audit itself so that they can correct it.
       6.	While all aspects are equally important, the following will  be specifically assessed during this audit
           a)	Action taken on deficiency reported in the previous audit.
           b)	Conduct of end semester exam of practical courses.
           c)	Award of internal assessment marks
           d)	Feedback from students about "Life Skills" course
           e)	Feedback from faculty on delivery of "Life skills “course
           f)	Internal assessment of Life Skills course
           g)	Minutes books of class/course committees, students grievance committee
           h)	Students activity register
           i)	Course diary of theory courses - Award of IA marks, Topics Coverage

       7.	Photographs(prepare as PPT by clearly mentioning the events name, date etc.) and videos of the activities,
          Association activities, NSS, sports, EDC,EECC, etc
       8.	Department level Grievance cell
       9.	College grievance cell file, Discipline and welfare committee etc.(Kind attention of the Members of 
          Faculty who are in charge of these committees)
       10.Details of Information of progress of students to the parents.
       11.Please take your own time to make a through audit in the department and check all documents that are
          covered in the guidelines as well as points specified in item-6.
       12.Please verify the following items
      </pre>
          <form action="{{ url('AUDITOR.form2') }}" method="POST">
            @csrf
            <center><button type="submit" class="btn btn-success" name="Start">Start</button></center>
          </form>>
            
        </div>
      
      </div>
  </div>

  <!-- footer -->
<footer class="container-fluid">
  <p><p>&copy Copyright Protected By BiGOne IT SolutionS</p></p>
</footer>
</div>    
</body>
</html>
