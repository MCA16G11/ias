<!doctype html>
<html>
  <head>
<link rel="shortcut icon" href="IASttl1.png"/>
  @include('headerfooter')
<link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
    <style>

      th, td {
        padding: 8px 20px;
        border: 1px solid #dddddd;
        text-align: left;

      }
     

    .container {
    width: 50%;
    display: horizontal;
    position: relative;
    padding-left: 35px;
    /* margin-bottom: 12px; */
    cursor: pointer;
    font-size: 12px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default radio button */
.container input {
    position: relative;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #eee;
    border-radius: 20%;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the indicator (dot/circle) when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the indicator (dot/circle) */
.container .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 30%;
	background: white;
}

      /* input {
        padding: 5px 10px;
        align: center;
        spacing:5px;
        height:80px;
      } */

      

      table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }
      tr:nth-child(even) {
        background-color: #dddddd;
      }
      input : checked{
        height: 50px;
        width: 50px;
      }
      .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
      }
      .container:hover input ~ .checkmark {
        background-color: #ccc;
      }
      .container input:checked ~ .checkmark {
        background-color: #2196F3;
      }
      .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }
    .btn1{
      padding: 30px 610px;
    }

    </style>
    <script type="text/javascript">
      $(document).ready(functon(){
        $('input[type="checkbox"]').click(function(){
          if(($this).is(":not(:checked)")){
            alert("checkbox is unchecked");

          }
        });
      });
    </script>

  </head>
  <body>
    <form action="{{ url('valuation') }}" method="post">
      @csrf
      <input type="hidden" name="departmentAudit" value="{{ $departmentAudit->id }}">
      <table border="1" cellspacing="0" style="100%" width="100%">
        <tr>
          <th colspan="8">Key Aspects Evaluation</th>
        </tr>
        <tr>
            <th>Key Aspects</th>
            <th>Group</th>
            <th>Marks</th>
            <th>Remarks/Observations</th>
        </tr>
          @foreach($departmentAudit->auditScore as $auditScore)
            <tr>
              <td width="40%"><label>{{ $auditScore->keyAspect->id }}. {{$auditScore->keyAspect->title}}</label></td>
              <td width="10%">{{ $auditScore->keyAspect->group }}</td>
              <td width="20%">
              <label class="container">5 Excellent<input type="Radio"  name="mark_{{ $auditScore->id }}" value="5"><span class="checkmark"></span></label>
              <label class="container">4 Good<input type="Radio" name="mark_{{ $auditScore->id }}" value="4"><span class="checkmark"></span></label>
              <label class="container">3 Fair<input type="Radio" name="mark_{{ $auditScore->id }}" value="3"><span class="checkmark"></span></label>
              <label class="container">2 Poor<input type="Radio" name="mark_{{ $auditScore->id }}" value="2"><span class="checkmark"></span></label>
              <label class="container">1 Very Poor<input type="Radio" name="mark_{{ $auditScore->id }}" value="1"><span class="checkmark"></span></label>
              </td>
              <td width="20%"><textarea id="textarea" name="remark_{{ $auditScore->id }}" style="width:220px"></textarea></td>
            </tr>
          @endforeach
        </table>
          <div class=btn1>
          <button type="submit" class="btn btn-success">Submit</button>
          </div>
      </form>
        <!-- footer -->
<footer class="container-fluid">
  <p><p>&copy Copyright Protected By BiGOne IT SolutionS</p></p>
</footer>
</div>
</body>
</html>
