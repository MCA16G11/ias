<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="shortcut icon" href="IASttl1.png"/>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
  <script src="{{ asset('js/app.js') }}" defer></script>
  <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->


  <style>
    .myNav{
      color-white;
    }

    .color{
      background-color: rgb(57, 142, 192);
      border:0px;
      color-white;
    }

    .top-right1 {
               position: absolute;
               width: 100px;
               height: 30px;
               right: 70px;
               top: 8px;
           }

    .homebtn {
                position: absolute;
                width: 100px
                height: 30px;
                left: 30px;
                top: 10px;
              }     
    .btn.btn-primary{
      background-color: rgb(57, 142, 192);
      border:0px;
      color-white; 
    }         

  </style>

</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class navbar-left>
    <a href="{{ url('main_home') }}" class="navbar-left"><img style="max-width:100px; margin-top: -1px; max-height:50px;" src="IASicon2.png"></a>
    </div>
    <!-- <form method="get" action="/">
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <div class="homebtn">
        <button type="submit" class="color"><span class="glyphicon glyphicon-home"></span> Home</button>
        </div>
      </ul>
    </form> -->
        <ul class="nav navbar-nav navbar-right">
        <div class="top-right1">
                            <li>
                                <a class="btn btn-primary" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                 {{ Auth::user()->fullname }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
      </div>
      </ul>
            @csrf
            <!-- <div class="top-right1"> -->
            <!-- <button type="submit" class="color"><span class="glyphicon glyphicon-log-out"></span> Logout</button> -->
            <!-- </div> -->
          </ul>

          <!-- </form> -->
        </li>
      </ul>
    </div>
  </div>
</nav>


</body>
</html>
