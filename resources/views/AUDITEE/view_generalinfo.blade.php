<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>GENERAL-INFO</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AUDITEE HOME</h4>
      <ul class="nav nav-pills nav-stacked">
      <!-- <li ><a href="{{ url('AUDITEE.auditee_profile') }}">Profile</a></li> -->
        <li><a href="{{ url('AUDITEE.notification') }}">Notifications</a></li>
        <li class="active"><a href="{{ url ('AUDITEE.view_generalinfo') }}">General Informations</a></li>
        <li><a href="{{ url('AUDITEE.view_checklist') }}">Checklist</a></li>
        <li><a href="{{ url('AUDITEE.view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('AUDITEE.view_report') }}">Audit Report</a></li> -->
      </ul><br>
    </div>

<!-- Table Content -->
    <div class="col-sm-9">
      <h4><small>GENERAL INFO</small></h4>
      <div class="hr">
      <hr>
      </div>
      <pre><b>
                    Note to the Departments   (Software Based Audit)</b>

      1.	Please prepare the Actions taken on the deficiency reported during the previous external Audit.
      2.	The audit is based on the Key Aspects mentioned by the guidelines given by KTU.
      3.	Key aspect evaluation o be on a five point scale as follows
          Excellent (5) 	Good (4) 	Fair (3) 		Poor (2) 	Very Poor (1)
      4.	Auditors’ Remarks / Observations will be informed to the department for rectification.
      5.	The Auditors will look for all aspects and the following will  be specifically assessed during this audit
          a)	Action taken on deficiency reported in the previous audit.
          b)	Conduct of end semester exam of practical courses.
          c)	Award of internal assessment marks
          d)	Feedback from students about "Life Skills" course
          e)	Feedback from faculty on delivery of "Life skills “course
          f)	Internal assessment of Life Skills course
          g)	Minutes books of class/course committees, students grievance committee
          h)	Students activity register
          i)	Course diary of theory courses - Award of IA marks, Topics Coverage
      6.	Please keep a printed list of Class/Course Committees with the members chaired by individual Departments for the support of Auditors.
      7.	Please keep a printed list of Class wise advisors
      8.	The Auditors’ may take long time for this audit.
      9.	The members of Faculty in charge of the following  in each department may kindly organize the photographs (prepare as PPT by clearly mentioning the events name, date etc.) and videos of the activities and programs  organized by them for audit by the concerned Auditors
          IEEE
          ISTE
          NSS – Unit-1 (No.191)
          NSS – Unit-2(No.602)
          EDC
          EECS
          Department Associations
          Sports
          Staff Advisor
      10.	 Department level Grievance cell
      11.	 College grievance cell file, Discipline and welfare committee etc.(Kind attention of the Members of Faculty who are in charge of these committees)
      12.	Details of Information of progress of students to the parents.
      13.	Please co-operate in all respect with the auditors.
      14.	Please make the rectifications suggested by the Auditors.

    </pre>

    </div>
  </div>


<!-- footer -->
<footer class="container-fluid">
  <p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>
</div>
</body>
</html>
