<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>CHECKLIST</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AUDITEE HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <!-- <li ><a href="{{ url('AUDITEE.auditee_profile') }}">Profile</a></li> -->
        <li><a href="{{ url('AUDITEE.notification') }}">Notifications</a></li>
        <li><a href="{{ url ('AUDITEE.view_generalinfo') }}">General Informations</a></li>
        <li class="active"><a href="{{ url('AUDITEE.view_checklist') }}">Checklist</a></li>
        <li><a href="{{ url('AUDITEE.view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('AUDITEE.view_report') }}">Audit Report</a></li> -->
      </ul><br>
    </div>

<!-- Table Content -->
    <div class="col-sm-9">
      <h4><small>CHECKLIST</small></h4>
      <div class="hr">
      <hr>
      </div>

     <div class="table">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>SI NO.</th>
                  <th>DOCUMENT NAME</th>
                  <th>SOFT/HARD COPY</th>
                </tr>
              </thead>
              <tbody>
                
                <tr>
                  <td>1a</td>
                  <td>Class Time Table</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>1b</td>
                  <td>Faculty Time Table</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>2</td>
                  <td>Students Roll List</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>3</td>
                  <td>Students Batch List ( for practical courses, projects & elective courses)</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>4</td>
                  <td>Minutes of course/class committees</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>*5</td>
                  <td>Course Diary for all the courses including practical, seminar, project etc.</td>
                  <td>Soft/Hard</td>
                </tr>

                <tr>
                  <td>*6</td>
                  <td>Course File (Samples in hard copies)</td>
                  <td>Soft/Hard</td>
                </tr>

                <tr>
                  <td>7</td>
                  <td>Tutorial Log book</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>8</td>
                  <td>Equipment Log register used in Laboratories</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>9</td>
                  <td>Consolidated Attendance statement of students</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>10</td>
                  <td>Consolidated statement of marks of internal tests</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>11</td>
                  <td>Seminar presentation details</td>
                  <td>Soft/Hardtd>
                </tr>

                <tr>
                  <td>12</td>
                  <td>Project (Mini project/Design project/Final semester project) progress review  reports</td>
                  <td>Soft/Hard</td>
                </tr>

                <tr>
                  <td>13</td>
                  <td>Register of internal evaluation marks</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>14</td>
                  <td>Student Activities Log Book( for B.Tech programme only)</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>15</td>
                  <td>Log book for summer and contact courses</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>16</td>
                  <td>Register of Remedial/Bridge/Language Lab classes</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>17</td>
                  <td>Minutes of Discipline, Academic and Student Welfare Committees</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>18</td>
                  <td>Consolidated semester grades of students</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>19</td>
                  <td>Result Analysis</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>20</td>
                  <td>Photographs(prepare as PPT by clearly mentioning the events name, date etc.) and videos of the activities , Association activities, NSS, sports, EDC,EECC, etc</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>21</td>
                  <td>Department level Grievance cell</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>22</td>
                  <td>College grievance cell file, Discipline and welfare committee etc.(Kind attention of the Members of Faculty who are in charge of these committees</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>23</td>
                  <td>Details of Information of progress of students to the parents</td>
                  <td>Soft/Hard</td>
                </tr> 

            <tr>
              <td colspan="8"><b>*5A: Course Diary for Lecture Based Courses Shall Contain</b>
              </td> 
            </tr>

                <tr>
                  <td>1</td>
                  <td>Time Schedule of classes</td>
                  <td>Soft</td>
                </tr> 

                <tr>
                  <td>2</td>
                  <td>Syllabus</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>3</td>
                  <td>Course plan</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>4</td>
                  <td>Year Calendar</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>5</td>
                  <td>Details of assignments, tutorials</td>
                  <td>Hard</td>
                </tr>

                <tr>
                  <td>6</td>
                  <td>Attendance of students</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>7</td>
                  <td>Marks awarded for assignments, internal exams etc</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>8</td>
                  <td>Internal evaluation marks</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>9</td>
                  <td>Topics covered and mode of instruction in each class</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>10</td>
                  <td>Extra classes engaged</td>
                  <td>Soft</td>
                </tr>

                <tr>
                  <td>11</td>
                  <td>Learning materials provided</td>
                  <td>Soft/Hard</td>
                </tr>



          </tbody>
        </table>
       </div>
     </div>          
    </div>
  </div>


<!-- footer -->
<footer class="container-fluid">
  <p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>
</div>
</body>
</html>
