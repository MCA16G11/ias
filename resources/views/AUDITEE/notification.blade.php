<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>AUDITEE-NOTIFICATIONS</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
  <style>
      .pos{

        position:relative;
        width: 1000px;
      }
  </style>
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AUDITEE HOME</h4>
      <ul class="nav nav-pills nav-stacked">
      <!-- <li ><a href="{{ url('AUDITEE.auditee_profile') }}">Profile</a></li> -->
        <li class="active"><a href="{{ url('AUDITEE.notification') }}">Notifications</a></li>
        <li><a href="{{ url ('AUDITEE.view_generalinfo') }}">General Informations</a></li>
        <li><a href="{{ url('AUDITEE.view_checklist') }}">Checklist</a></li>
        <li><a href="{{ url('AUDITEE.view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('AUDITEE.view_report') }}">Audit Report</a></li> -->
      </ul><br>
    </div>

<!-- Table Content -->
    <div class="col-sm-9">
      <h4><small>NOTIFICATIONS</small></h4>
      <div class="hr">
      <hr>
      </div>

      <div class="pos">
           <table class="table table-bordered">
           <thead>
              <tr>
                <th>ID</th>
                <th>NOTIFICATION</th>
                <th>DATE</th>
              </tr>
            </thead>
            <tbody>
              @foreach(App\notification::all() as $audit)
              <tr>
                <td>{{$audit->id}}</td>
                <td>{{$audit->notification}}</td>
                <td>{{$audit->created_at}}</td>
              </tr>
              @endforeach
            </tbody>
           </table>
           </div>
    </div>
  </div>
</div>

<!-- footer -->
<footer class="container-fluid">
  <p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>
</div>
</body>
</html>
