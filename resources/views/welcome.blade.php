
<!doctype html>
<html>
<head>
<title>IASMESCE</title>

<link rel="shortcut icon" href="IASttl1.png"/>
<!-- Load an icon library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/welcome.css">
</head>
<style>
html, body{
/* background-color: lightblue;; */
background-image: url(audit3.jpg);
               background-size: cover;
               opacity: inherit;
               color: #636b6f;
               font-family: 'Raleway', sans-serif;
               font-weight: 100;
               height: 100vh;
               margin: 0;
           }
</style>

<body>

<!-- Navbar -->
<div class="navbar">
  <a class="active" href="{{url('/')}}"><i class="fa fa-fw fa-home"></i> Home</a>
  <!-- <a href="{{url('/about')}}">About</a>
  <a href="{{url('/contact')}}">Contact</a> -->
  <div class="top-right">
  <button class="open-button"onclick="openForm()"><i class="fa fa-fw fa-user"></i>Login</button>
 <!-- <a href="{{url('/login')}}"><i class="fa fa-fw fa-user"></i> Login</a> -->
  </div>
 <!-- <a href="{{url('/aaa')}}">aaa</a> -->
</div>

<!-- Logo Aniimation -->
<div class="content">
<div class="container">
<div class="flip-card">
    <div class="flip-card-inner">
     <div class="flip-card-front">
     <img src="IASLogo.png" width="250" height="200" align: "center">
    </div>
     <div class="flip-card-back">
      <p>Welcome to</p>
     <h1>Internal Auditing System</h1>
      <p>-The team that ticks all</p>
      <p>the boxes-</p>
       </div>
     </div>
    </div>



<!-- Login Form -->
<div class="form-popup" id="myForm">
  <form action="{{ route('loginCustom' )}}" class="form-container" method="POST">
    @csrf
    <h1>Login</h1>
<!-- <div class="usertype">
<label>
    <input type="radio" name="usertype" value="STAFF"> STAFF
    <input type="radio" name="usertype" value="IQAC"> IQAC
    <input type="radio" name="usertype" value="AUDITEE"> AUDITEE
    <input type="radio" name="usertype" value="AUDITOR"> AUDITOR<br></br>
</label>
</div> -->

    <label for="email"><b>Email</b></label>
    <input type="text" placeholder="Enter Email" name="email" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="psw" required>


    <button type="submit" class="btn">Login</button>

    <div class="forgot-pass">
    <a href="#">Forgot password.?</br>
    </div>

    <button type="button" class="btn cancel" onclick="closeForm()">X</button>
  </form>
</div>


<div id="mySidenav" class="sidenav">
  <a href="{{url('/about')}}" id="about"><i class="fa fa-info-circle"></i> About</a>
  <a href="{{url('/register1')}}" id="register"><i class="fa fa-address-card"></i> Register</a>
  <a href="{{url('/AMR.amrhome')}}" id="projects"><i class="fa fa-calendar"></i> Calendar</a>
  <a href="{{url('/contact')}}" id="contact"><i class="fa fa-address-book"></i> Contact</a>
</div>


              <!-- want to remove after testing -->
               <a href="{{url('/AUDITEE.auditee_home')}}" id="auditee">AUDITEE</a>
               <a href="{{url('/IQAC.iqac_home')}}" id="iqac">IQAC</a>
               <a href="{{url('/AUDITOR.auditor_home')}}" id="auditor">AUDITOR</a>
               <a href="{{url('/IQACCo.iqacco_home')}}" id="iqacco">IQACCo</a>


<!-- Footer -->
  <div class="footer">
  <p>&copy Copyright Protected by BiGOne IT SolutionS</p>
  </div>


<!-- Log in Form JS -->
<script>

function openForm() {
    document.getElementById("myForm").style.display = "block";
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}

</script>

</body>
</html>
