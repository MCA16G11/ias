<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>IQACCo-NOTIFICATION</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>IQAC Co. HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <!-- <li><a href="{{ url('IQACCo.iqacco_profile') }}">Profile</a></li> -->
        <li class="active"><a href="{{ url('IQACCo.send_notification') }}">Send Notifications</a></li>
        <li><a href="{{ url('IQACCo.view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('IQACCo.view_auditors') }}">View Auditors</a></li> -->
        <li><a href="{{ url('IQACCo.view_report') }}">Audit Report</a></li>
      </ul><br>
    </div>

    @if ($message = Session::get('msg'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
              <strong>{{ $message }}</strong>
      </div>
      @endif

      @if ($errors->any())
          <div class="alert alert-danger">
          <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif

    <!-- Table Head -->
    <div class="col-sm-9">
      <h4><small>SEND NOTIFICATION</small></h4>
      <div class="hr">
      <hr>
      </div>

    <!-- Table Content -->
      <div class="form1">
        <form action="{{ url('sendNotification') }}" method="POST">
          @csrf
        <div>
        <textarea id="notificationsubject" name="notificationsubject" placeholder="Write something.." style="width:490px" style="height:200px"></textarea>
        </div>
         <button type="submit" class="btn btn-success">Send Notification</button>
       </form>
            </div>
          </div>
      </div>


<!-- footer -->
<footer class="container-fluid">
  <p><p>&copy Copyright Protected By BiGOne IT SolutionS</p></p>
</footer>

</body>
</html>
