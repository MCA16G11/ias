<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>IQACCo-AUDITORS-LIST</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>IQAC Co. HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="{{ url('IQACCo.iqacco_profile') }}">Profile</a></li>
        <li><a href="{{ url('IQACCo.send_notification') }}">Send Notifications</a></li>
        <li><a href="{{ url('AMR.view_schedule') }}">View Schedule</a></li>
        <li calss="active"><a href="{{ url('IQACCo.view_auditors') }}">View Schedule</a></li>
        <li><a href="{{ url('AMR.view_report') }}">Audit Report</a></li>
      </ul><br>
    </div>

<!-- Table Content -->
    <div class="col-sm-9">
      <h4><small>AUDITORS LIST</small></h4>
      <div class="hr">
      <hr>
      </div>

    <div class="table">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>NAME</th>
                  <th>E MAIL</th>
                  <th>DEPARTMENT</th>
                  <!-- <th></th> -->
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>101</td>
                  <td>Shabeer</td>
                  <td>shabeer123@gmail.com</td>
                  <td>MCA</td>
                  <!-- <td><button type="button" class="btn btn-warning">CHANGE</button> -->
                </tr>

                <tr>
                  <td>102</td>
                  <td>Anjali</td>
                  <td>anjali3@gmail.com</td>
                  <td>EC</td>
                  <!-- <td><button type="button" class="btn btn-warning">CHANGE</button> -->
                </tr>

                <tr>
                  <td>103</td>
                  <td>Rifana M T</td>
                  <td>rifanamt@gmail.com</td>
                  <td>EEE</td>
                  <!-- <td><button type="button" class="btn btn-warning">CHANGE</button> -->
                </tr>
              </tbody>
            </table>
          </div>

    </div>
  </div>


<!-- footer -->
<footer class="container-fluid">
  <p><p>&copy Copyright Protected By BiGOne IT SolutionS</p></p>
</footer>
</div>
</body>
</html>
