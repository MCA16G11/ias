<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>IQACCo-AUDIT-REPORT</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>IQAC Co. HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <!-- <li><a href="{{ url('IQACCo.iqacco_profile') }}">Profile</a></li> -->
        <li><a href="{{ url('IQACCo.send_notification') }}">Send Notifications</a></li>
        <li><a href="{{ url('AMR.view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('IQACCo.view_auditors') }}">View Auditors</a></li> -->
        <li class="active"><a href="{{ url('AMR.view_report') }}">Audit Report</a></li>
      </ul><br>
    </div>

<!-- Table Content -->
    <div class="col-sm-9">
      <h4><small>REPORT VIEW</small></h4>
      <div class="hr">
      <hr>
      </div>

    <div class="dropdown">
      <form action="{{ url('resultIQACCo') }}" class="form-inline" method="post">
        @csrf
      <div class="form-group">
      <label for="dept">SELECT AUDIT NO.
        <select class="form-control" name="college_audit">
          @foreach(App\CollegeAudit::all() as $auditnum)
              <option value="{{$auditnum->id}}" @isset($CollegeAudit) @if($auditnum == $CollegeAudit) selected @endif @endisset >{{$auditnum->audit_no}}</option>
          @endforeach
        </select>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>

    </div>

    @isset($CollegeAudit)
      

    <!-- Table Content -->
    <div class="table">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>DEPARTMENT</th>
                  <th>VIEW REPORT</th>
                </tr>
              </thead>
            <tbody>
          
              
              @foreach($CollegeAudit->departmentAudit as $departmentAudit)
                <tr>
                  <td>{{$departmentAudit->department->id}}</td>
                  <td>{{$departmentAudit->department->name}}</td>
                  <td>
                  <form action="{{ url('view_reportIqacco') }}" class="form-inline" method="POST"> 
                   @csrf
                  <input type="hidden" name="departmentAudit" value="{{$departmentAudit->id}}">
                  <div class="form-group">
                  <button type="submit" name="action" class="btn btn-warning"><i class="fa fa-eye"></i> VIEW REPORT</button>
                  </div>
                  </form>
                  </td>
                </tr>
               @endforeach     
              </tbody>
            </table>
    
    </div>
    @endisset
    </div>
  </div>
</div>

    </div>
  </div>

<!-- footer -->
<footer class="container-fluid">
<p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>
</div>
</body>
</html>
