<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>AMR-PENDING-LIST</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AMR HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li class="active"><a href="{{ url('pending_list') }}">Pending List</a></li>
        <li><a href="{{ url('staff_list') }}">Staff List</a></li>
        <li><a href="{{ url('create_audit') }}">Create Audit</a></li>
        <li><a href="{{ url('select_auditor') }}">Select Auditor</a></li>
        <li><a href="{{ url('view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('AMR.auditor_list') }}">Auditor List</a></li> -->
        <li><a href="{{ url('notification') }}">Notifications</a></li>
        <li><a href="{{ url('view_messages') }}">Messages</a></li>
        <li><a href="{{ url('view_report') }}">View Report</a></li>
      </ul><br>
    </div>

    <!-- Table Head -->
    <div class="col-sm-9">
      <h4><small>PENDING LIST</small></h4>
      <div class="hr">
      <hr>
      </div>
      @include('flash_message')
    <!-- Table Content -->
    <div class="table">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>STAFF ID</th>
                  <th>FIRST NAME</th>
                  <th>LAST NAME</th>
                  <th>EMAIL</th>
                  <th>DEPARTMENT</th>
                  <th>YEAR</th>
                  <th>STATUS</th>
                </tr>
              </thead>
              <tbody>
                @foreach ( $registers as $register )
                <tr>
                  <td>{{ $register->id }}</td>
                  <td>{{ $register->staffid }}</td>
                  <td>{{ $register->name }}</td>
                  <td>{{ $register->lname }}</td>
                  <td>{{ $register->email }}</td>
                  <td>{{ $register->department->name }}</td>
                  <td>{{ $register->year }}</td>
                  <td><a href="{{url('update').'/'.$register->id}}" class="btn btn-success"><i class="fa fa-check"></i> Approve</a>
                  <a href="{{url('destroy').'/'.$register->id}}" class="btn btn-danger"><i class="fa fa-close"></i> Reject</a></td>

                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>

    </div>
  </div>



<!-- footer -->
<footer class="container-fluid">
  <p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>

</body>
</html>
