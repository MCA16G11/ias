<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>AMR-REPORT-VIEW</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">

  <style>
  .center{
    text-align:center;
  }
  .btn1{
    position: absolute;
    top: 70px;
    left: 985px; 
  }

  </style>
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AMR HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="{{ url('pending_list') }}">Pending List</a></li>
        <li><a href="{{ url('staff_list') }}">Staff List</a></li>
        <li><a href="{{ url('create_audit') }}">Create Audit</a></li>
        <li><a href="{{ url('select_auditor') }}">Select Auditor</a></li>
        <li><a href="{{ url('view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('AMR.auditor_list') }}">Auditor List</a></li> -->
        <li><a href="{{ url('notification') }}">Notifications</a></li>
        <li><a href="{{ url('view_messages') }}">Messages</a></li>
        <li class="active"><a href="{{ url('view_report') }}">View Report</a></li>
      </ul><br>
    </div>

    <!-- Table Head -->
    <div class="col-sm-9">
      <h4><small>REPORT VIEW</small></h4>
      <div class="hr">
      <hr>
      </div>


    
    <!-- <a href="{{ url('main_home') }}" class="navbar-left"><img style="max-width:120px; margin-top: -66px; margin-left: 470px; max-height:120px;" src="mesce.png"></a> -->
    <!-- Table Content -->
    <div class="table">
            <table class="table table-bordered">
              <thead>
                <tr>
                <th colspan="1"><img style="max-width:150px; margin-top: -10px; margin-left: 20px; max-height:130px;" src="mesce1.png"></th>
                <th colspan="2"><h2 class="center">MES COLLEGE OF ENGINEERING</h2><h4 class="center">KUTTIPPURAM</h4><h5 class="center">KTU-INTERNAL AUDIT REPORT</h5></th>
                <th colspan="2"><h4>Department: {{$departmentAudit->department->name}}</h4></th>
                </tr>
                <tr>
                  <th>ID</th>
                  <th>KEY ASPECT</th>
                  <th>GROUP</th>
                  <th>SCORE</th>
                  <th>REMARKS</th>
                </tr>

              @foreach($departmentAudit->auditScore as $auditScore)
                <tr>
                  <td>{{ $auditScore->keyAspect->id }}</td>
                  <td>{{ $auditScore->keyAspect->title }}</td>
                  <td>{{ $auditScore->keyAspect->group }}</td>
                  <td>{{ $auditScore->mark }}</td>
                  <td>{{ $auditScore->remark }}</td>
                </tr>
              @endforeach
              </thead>
            </table>
            </div>
            <div class="btn1">
          <a href="{{ url('report/Pdf/'.$departmentAudit->id) }}" class="btn btn-danger" target="_blank"><i class="fa fa-download"></i> Export</a>
          
        </div>
    </div>
  </div>
</div>
<footer class="container-fluid">
  <p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>
</body>
</html>
