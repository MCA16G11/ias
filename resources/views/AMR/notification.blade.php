<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>AMR-NOTIFICATION</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
   <style>
      .pos{

        position:relative;
        width: 1000px;
      }
  </style>
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AMR HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="{{ url('pending_list') }}">Pending List</a></li>
        <li><a href="{{ url('staff_list') }}">Staff List</a></li>
        <li><a href="{{ url('create_audit') }}">Create Audit</a></li>
        <li><a href="{{ url('select_auditor') }}">Select Auditor</a></li>
        <li><a href="{{ url('view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('AMR.auditor_list') }}">Auditor List</a></li> -->
        <li class="active"><a href="{{ url('notification') }}">Notifications</a></li>
        <li><a href="{{ url('view_messages') }}">Messages</a></li>
        <li><a href="{{ url('view_report') }}">View Report</a></li>
      </ul><br>
    </div>

    <!-- Table Head -->
    <div class="col-sm-9">
      <h4><small>NOTIFICATION</small></h4>
      <div class="hr">
      <hr>
      </div>

    <!-- Table Content -->
     <div class="pos">
           <table class="table table-bordered">
           <thead>
              <tr>
                <th>ID</th>
                <th>NOTIFICATION</th>
                <th>DATE</th>
              </tr>
            </thead>
            <tbody>
              @foreach(App\notification::all() as $audit)
              <tr>
                <td>{{$audit->id}}</td>
                <td>{{$audit->notification}}</td>
                <td>{{$audit->created_at}}</td>
              </tr>
              @endforeach
            </tbody>
           </table>
           </div>

    </div>
  </div>
</div>


<!-- footer -->
<footer class="container-fluid">
  <p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>

</body>
</html>
