<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>AMR-SELECT-AUDITOR</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AMR HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="{{ url('AMR.pending_list') }}">Pending List</a></li>
        <li><a href="{{ url('AMR.staff_list') }}">Staff List</a></li>
        <li><a href="{{ url('AMR.create_audit') }}">Create Audit</a></li>
        <li><a href="{{ url('AMR.view_schedule') }}">View Schedule</a></li>
        <li class="active"><a href="{{ url('AMR.select_auditor') }}">Select Auditor</a></li>
        <li><a href="{{ url('AMR.auditor_list') }}">Auditor List</a></li>
        <li><a href="{{ url('AMR.notification') }}">Notification</a></li>
        <li><a href="{{ url('AMR.view_messages') }}">Messages</a></li>
        <li><a href="{{ url('AMR.view_report') }}">View Report</a></li>
      </ul><br>
    </div>

    <!-- Table Head -->
    <div class="col-sm-9">
      <h4><small>SELECT AUDITOR</small></h4>
      <div class="hr">
      <hr>
      </div>

      <div class="dropdown">
                      <form action="{{url('show')}}" class="form-inline" method="post">
                        <div class="form-group">
                          <label for="dept">SELECT DEPARTMENT

                            <select  name="dept" class="form-control">

                              @foreach(App\Department::all() as $department)
                                 <option value="{{$department->id}}">{{$department->name}}</option>
                              @endforeach
                            </select>
                            <select class="form-control" name="staffs">
                              @foreach(App\User::where('status','Approved')->get() as $user)
                                 <option value="{{$department->id}}">{{$user->fullname}}</option>
                              @endforeach
                            </select>
                      </div>
                       <div class="form-group">
                         <button type="submit" class="btn btn-primary">Submit</button>
                       </div>
                </form>
                </div>

    <!-- Table Content -->
    <div class="table">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>NAME</th>
                  <th>EMAIL</th>
                  <th>DEPARTMENT</th>
                  <th>ACTION</th>
                </tr>
              </thead>
              <tbody>
              @if(count($registers) > 0)
              @foreach ( $registers as $register )
                <tr>
                  <td>{{ $register->id }}</td>
                  <!-- <td>{{ $register->staffid }}</td> -->
                  <td>{{ $register->fullname }}</td>
                  <td>{{ $register->email }}</td>
                  <td>{{ $register->department->name }}</td>
                  
                  <td><button type="button" class="btn btn-success">Set Auditor</button></td>
                
                </tr>
                @endforeach
                @endif
                <!-- <tr>
                  <td>s101</td>
                  <td>Ramesh</td>
                  <td>Ramesh@yahoo.com</td>
                  <td>2001</td>
                  <td><button type="button" class="btn btn-success">Set Auditor</button></td>
                </tr>

                <tr>
                  <td>s198</td>
                  <td>Shilpa</td>
                  <td>kshilpa@gmail.com</td>
                  <td>2011</td>
                  <td><button type="button" class="btn btn-success">Set Auditor</button></td>
                </tr>

                <tr>
                  <td>s210</td>
                  <td>Vyshakh</td>
                  <td>vyshakh234@gmail.com.com</td>
                  <td>2014</td>
                  <td><button type="button" class="btn btn-success">Set Auditor</button></td>
                </tr> -->

              </tbody>
            </table>

    </div>
  </div>
</div>


<!-- footer -->
<footer class="container-fluid">
  <p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>

</body>
</html>
