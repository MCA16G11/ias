<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>AMR-STAFF-LIST</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AMR HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="{{ url('pending_list') }}">Pending List</a></li>
        <li class="active"><a href="{{ url('staff_list') }}">Staff List</a></li>
        <li><a href="{{ url('create_audit') }}">Create Audit</a></li>
        <li><a href="{{ url('select_auditor') }}">Select Auditor</a></li>
        <li><a href="{{ url('view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('AMR.auditor_list') }}">Auditor List</a></li> -->
        <li><a href="{{ url('notification') }}">Notifications</a></li>
        <li><a href="{{ url('view_messages') }}">Messages</a></li>
        <li><a href="{{ url('view_report') }}">View Report</a></li>
      </ul><br>
    </div>

    <!-- Table Head -->

    <div class="col-sm-9">
      <h4><small>STAFF LIST</small></h4>
      <div class="hr">
      <hr>
      </div>

    @include('flash_message')
    @if(Session::has('msg'))
         <div class="alert alert-success" role="alert">
           {{session::get('msg')}}
         </div>
         @endif
    <!-- Table Content -->
    <div class="table">
            <table class="table table-bordered">
              <thead>
                <th>ID</th>
                <!-- <th>STAFF ID</th> -->
                <th>NAME</th>
                <th>EMAIL</th>
                <th>DEPARTMENT</th>
                <th>ROLE</th>
                <th>YEAR</th>
                <th>SET ROLE</th>
              </tr>
            </thead>
            <tbody>
            @if(count($registers) > 0)
              @foreach ( $registers as $register )
                <tr>
                  <td>{{ $register->id }}</td>
                  <!-- <td>{{ $register->staffid }}</td> -->
                  <td>{{ $register->fullname }}</td>
                  <td>{{ $register->email }}</td>
                  <td>{{ $register->department->name }}</td>
                  <td>{{ $register->role_names}}</td>
                  <td>{{ $register->year }}</td>
                  <!--
                  <td><a href="{{url('update1').'/'.$register->id}}" class="btn btn-success">IQAC</a>
                  <a href="{{url('update2').'/'.$register->id}}" class="btn btn-success">AUDITEE</a></td> -->
                  <td>
                  
                  <div class="dropdown">
                      <form action="{{url('role/store')}}" class="form-inline" method="post"> 
                      @csrf
                        <div class="form-group">
                          <input type="hidden" name="user" value="{{ $register->id }}">
                            <select  name="role" class="form-control">
                              @foreach(App\Role::where('name','!=','AMR')->get() as $role)
                                 <option value="{{$role->id}}">{{$role->name}}</option>
                              @endforeach
                            </select>
                      </div>
                       <div class="form-group">
                          <button type="submit" name="action" value="attach" class="btn btn-success"><i class="fa fa-plus-square"></i> Set</button>
                         <button type="submit" name="action" value="detach" class="btn btn-danger"><i class="fa fa-minus-square"></i> Reset</button>
                       </div>
                  </form>
                </div>
                  </td>

                </tr>
                @endforeach
              @else
               <p>No data found</p>
              @endif
              </tbody>
            </table>
            </div>
          </div>
      </div>


<!-- footer -->
<footer class="container-fluid">
  <p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>

</body>
</html>
