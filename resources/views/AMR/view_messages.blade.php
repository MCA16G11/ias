<!DOCTYPE html>
<html lang="en">
<head>
  @include('headerfooter')
  <link rel="shortcut icon" href="IASttl1.png"/>
  <title>AMR-MESSAGES</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/amrstyle.css">
  <link rel="stylesheet" href="/css/amr.css">
</head>
<body>

<!-- Side Navbar -->
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>AMR HOME</h4>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="{{ url('pending_list') }}">Pending List</a></li>
        <li><a href="{{ url('staff_list') }}">Staff List</a></li>
        <li><a href="{{ url('create_audit') }}">Create Audit</a></li>
        <li><a href="{{ url('select_auditor') }}">Select Auditor</a></li>
        <li><a href="{{ url('view_schedule') }}">View Schedule</a></li>
        <!-- <li><a href="{{ url('AMR.auditor_list') }}">Auditor List</a></li> -->
        <li><a href="{{ url('notification') }}">Notifications</a></li>
        <li class="active"><a href="{{ url('view_messages') }}">Messages</a></li>
        <li><a href="{{ url('view_report') }}">View Report</a></li>
      </ul><br>
    </div>

    <!-- Table Head -->
    <div class="col-sm-9">
      <h4><small>MESSAGES</small></h4>
      <div class="hr">
      <hr>
      </div>

      <!-- Table Content -->
    <!-- table for displaying messages -->
  <div class="table">
      <table class="table table-bordered">
        <thead class="">
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Message</th>
            <th>ACTION</th>
          </tr>
        </thead>
        <tbody>
          <!-- retrieving date from messages table -->
          @foreach(App\Message::all() as $message)
          <tr>
             <td>{{$message->id}}</td>
             <td>{{$message->name}}</td>
             <td>{{$message->email}}</td>
             <td>{{$message->message}}</td>
             <td>
                <form action="#">
                <div class="form-group">
                       <a href="{{url('reply').'/'.$message->id}}" class="btn btn-info"><i class="fa fa-reply"></i> Reply</a>
                       <a href="{{url('delete').'/'.$message->id}}" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</a>
                       <!-- <button type="submit" name="action" value="reply" class="btn btn-info"><i class="fa fa-reply"></i> Reply</button> -->
                       <!-- <button type="submit" name="action" value="delete" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button> -->
                       </div>
                </form>
              </td>      
          </tr>
          @endforeach
          <!-- end -->
        </tbody>
      </table>
    </div>

    </div>
  </div>
</div>


<!-- footer -->
<footer class="container-fluid">
  <p>&copy Copyright Protected By BiGOne IT SolutionS</p>
</footer>

</body>
</html>
