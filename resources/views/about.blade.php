

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>About</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <link rel="stylesheet" href="css/style.css">  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <style>
    
/* Style the navbar */
#navbar {
  overflow: hidden;
  background-color: #333;
}

/* Navbar links */
#navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px;
  text-decoration: none;
}

/* Page content */
.content {
  padding: 120px 500px;
}

/* The sticky class is added to the navbar with JS when it reaches its scroll position */
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}

/* Add some top padding to the page content to prevent sudden quick movement (as the navigation bar gets a new position at the top of the page (position:fixed and top:0) */
.sticky + .content {
  padding-top: 60px;
}

/* Style the navigation bar */
.navbar {
  width: 100%;
  background-color: #0008;
  overflow: auto;
}

/* Navbar links */
.navbar a {
  float: left;
  text-align: center;
  padding: 12px;
  color: white;
  text-decoration: none;
  font-size: 17px;
}

/* Navbar links on mouse-over */
.navbar a:hover {
  background-color: #000;
}

/* Current/active navbar link */
.active {
  background-color: skyblue;
}

/* Add responsiveness - will automatically display the navbar vertically instead of horizontally on screens less than 500 pixels */
@media screen and (max-width: 500px) {
  .navbar a {
    float: none;
    display: block;
  }
}


.footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    height: 6%;
    background-color: #0008;
    color: black;
    text-align: center;
}
  </style>
  
  </head>
  <body>

<!-- Header -->
<div class="navbar">
  <a href="{{url('/')}}"><i class="fa fa-fw fa-home"></i> Home</a> 
 </div>  

    <div class="container">
      <h1 class="ias">IAS</h1>
        <p>The Departments of any institution are the backbone of the core business of any institution where trifocal activities (teaching, learning, research and service) are executed. Academic audit is a mechanism to examine the overall quality of an institution and subsequence improvements to be made based on the observations reported. It is a continuous process of self-introspection for better growth of the institution. Here we introduce an INTERNAL AUDITING SYSTEM, a web application to enhance the activities associated with it</p>
        <p>IAS provides a systematic process of scheduling, implementing, monitoring and reviewing the quality of academic system. The selection of auditor and auditee, audit process and report generation can be performed using this system. Through this project we can reduce the paper work and time, effectively communicate and made easy to use.</p>

    </div>

<!-- Footer -->>
<div class="footer">
  <p>&copy Copyright protected by BiGOne IT SolutionS</p>
  </div>


  </body>
</html>
