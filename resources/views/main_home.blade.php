<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    @include('headerfooter')
    <link rel="shortcut icon" href="IASttl1.png"/>
    <title>HOME</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/amrstyle.css">
    <link rel="stylesheet" href="/css/amr.css">
  </head>
  <body>

    <!-- Side Navbar -->
    <div class="container-fluid">
      <div class="row content">
        <div class="col-sm-3 sidenav">
        <h4>ROLES</h4>
          <ul class="nav nav-pills nav-stacked">
          @if(Auth::user()->is_amr)
            <li ><a href="{{ url('amrhome') }}">AMR</a></li>
          @endif
          @if(Auth::user()->is_iqacco)
            <li><a href="{{ url('IQACCo.iqacco_home') }}">IQAC CO.</a></li>
          @endif
          @if(Auth::user()->is_iqac)
            <li ><a href="{{ url('IQAC.iqac_home') }}">IQAC</a></li>
          @endif
          @if(Auth::user()->is_auditee)
            <li ><a href="{{ url('AUDITEE.auditee_home') }}" @isset($menu) @if($menu == 'profile')  class="active" @endif @endisset>AUDITEE</a></li>
          @endif
          @if(Auth::user()->is_auditor)
            <li ><a href="{{ url('AUDITOR.auditor_home') }}">AUDITOR</a></li>
          @endif
          </ul><br>
        </div>
      </div>
      <main class="py-4">
          @yield('content')
      </main>
    </div>
    <footer class="container-fluid">
      <p> &copy copyright protected By BiGOne IT SolutionS</p>
    </footer>
  </body>
</html>
